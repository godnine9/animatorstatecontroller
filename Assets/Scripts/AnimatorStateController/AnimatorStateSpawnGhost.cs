using UnityEngine;
using System.Collections;

public class AnimatorStateSpawnGhost : AnimatorStateBase
{

    #region Method
    public override void OnTrigger(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<GhostShaderTest>().SpawnGhost();
    }
    #endregion
}