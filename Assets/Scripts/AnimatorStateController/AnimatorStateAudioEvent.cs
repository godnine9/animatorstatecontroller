using UnityEngine;
using System.Collections;

public class AnimatorStateAudioEvent : AnimatorStateBase
{
    #region Property
    public int  eventID = 0;
    public bool DoNotLoop = false;
    #endregion

    #region Method
    public override void OnTrigger(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
    #endregion
}