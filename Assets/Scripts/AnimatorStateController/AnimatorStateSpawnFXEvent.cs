using UnityEngine;
using System.Collections;

public class AnimatorStateSpawnFXEvent : AnimatorStateBase
{
    #region Property
    public GameObject VFXPrefabe;
    public string AttechmentName;

    private Transform CatchTransForm;
    #endregion

    #region Method
    public override void OnTrigger(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!string.IsNullOrEmpty(AttechmentName))
        {
            CatchTransForm = animator.transform;
            GameObject AttechmentPart = CatchTransForm.gameObject.FindChildByName(AttechmentName, true, true, false);
            Instantiate(VFXPrefabe,AttechmentPart.transform.position, Quaternion.identity);
        }
    }
    #endregion
}