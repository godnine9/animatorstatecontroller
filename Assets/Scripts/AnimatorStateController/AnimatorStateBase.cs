using UnityEngine;
using UnityEngine.Animations;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Animations;

public class AnimatorStateEventBehaviourCopier
{
    public static AnimatorStateBase CopiedBehaviours = null;
}
#endif

public class AnimatorStateBase : StateMachineBehaviour
{
#region Property
    [Tooltip("Normalized event trigger animation time (0~1)")]
	[Range (0.0f, 1.0f)]
    public float triggerPercentage = 0.0f;
#endregion

    private int               currentLayerIndex   = -1;
    private int               currentStateHash    = 0;
    private int               lastLoopTime        = 0;
    private bool              isCurrLoopTriggered = false;
    private bool              isRunning           = false;
    private bool              isEnterMustTrigger  = false;
    private AnimatorStateInfo currentStateInfo;

    protected bool IsEnterMustTrigger
    {
        get
        {
            return isEnterMustTrigger;
        }
    }

    public void CheckInterrupted(Animator animator, int layerIndex, AnimatorStateInfo currInfo, AnimatorStateInfo nextInfo)
    {
        if(!isRunning)
        {
            return;
        }

        if(currentLayerIndex != layerIndex)
        {
            return;
        }

        if(currentStateHash == currInfo.fullPathHash || (nextInfo.fullPathHash != 0 && currentStateHash == nextInfo.fullPathHash))
        {
            return;
        }

        OnExit(animator, currentStateInfo, currentLayerIndex, true);
        isRunning = false;
    }

#region Method
	public virtual void OnInit(Animator animator)
	{
	}

	public virtual void OnActive(Animator animator)
	{
	}

	public virtual void OnExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, bool isInterrupted)
	{
	}

	public virtual void OnTrigger(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
	}

	public virtual void OnUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
	}

	public virtual void OnRelease()
	{
	}
#endregion

#region UnityFollow
    private void OnExitInternal(Animator animator, AnimatorStateInfo currentStateInfo, AnimatorStateInfo nextStateInfo, int layerIndex, bool isInterrupted)
    {
        if (!isRunning)
        {
            return;
        }
        if (isCurrLoopTriggered == false)
        {
            isEnterMustTrigger = true;
            OnTrigger(animator, currentStateInfo, layerIndex);
        }
        OnExit(animator, currentStateInfo, layerIndex, isInterrupted);
        isRunning = false;
    }

    public sealed override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        isRunning = true;
        currentStateHash = stateInfo.fullPathHash;
        currentStateInfo = stateInfo;
        currentLayerIndex = layerIndex;
        isCurrLoopTriggered = false;
        lastLoopTime = 0;   
        isEnterMustTrigger = false;
    }

    public sealed override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
    {
        // Do nothing
    }

    public sealed override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
        AnimatorStateInfo nextStateInfo = animator.GetNextAnimatorStateInfo(layerIndex);
        OnExitInternal(animator, stateInfo, nextStateInfo, layerIndex, true);
	}

    public sealed override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
    {
        // Do nothing
    }

    public sealed override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
        currentStateInfo = stateInfo;
        
        if(!isRunning)
		{
            return;
		}

        OnUpdate(animator, stateInfo, layerIndex);

        // Calculate loop time and animation time
        bool aIsLooping = stateInfo.loop;
		int timeDecimalPart = (int)stateInfo.normalizedTime;
		float timeFractionPart = stateInfo.normalizedTime - timeDecimalPart;

		// Check loop 
		if(aIsLooping && timeDecimalPart > lastLoopTime && timeFractionPart != 0f)
		{			
			isCurrLoopTriggered = false;
			lastLoopTime = timeDecimalPart;
		}

		// Transition process
		if(animator.IsInTransition(layerIndex))
		{
            AnimatorStateInfo nextStateInfo = animator.GetNextAnimatorStateInfo(layerIndex);
            AnimatorTransitionInfo transitionInfo = animator.GetAnimatorTransitionInfo(layerIndex);
            if(nextStateInfo.fullPathHash != stateInfo.fullPathHash || (transitionInfo.anyState && nextStateInfo.normalizedTime != stateInfo.normalizedTime))
            {
                OnExitInternal(animator, stateInfo, nextStateInfo, layerIndex, transitionInfo.anyState);
                return;
            }
		}

		// Normal process
		if(!isCurrLoopTriggered && timeFractionPart >= triggerPercentage)
		{
			OnTrigger(animator, stateInfo, layerIndex);
			isCurrLoopTriggered = true;
		}
	}

    public sealed override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
    {
        // Do nothing
    }
    #endregion

#region Unity Editor Method
#if UNITY_EDITOR
    [ContextMenu("Copy")]
    protected void Copy()
    {
        AnimatorStateEventBehaviourCopier.CopiedBehaviours = CreateInstance(GetType()) as AnimatorStateBase;
        EditorUtility.CopySerialized(this, AnimatorStateEventBehaviourCopier.CopiedBehaviours);
    }

    [ContextMenu("Paste as Value")]
    protected void PasteAsValue()
    {
        if(AnimatorStateEventBehaviourCopier.CopiedBehaviours == null || AnimatorStateEventBehaviourCopier.CopiedBehaviours.GetType() != GetType())
        {
            return;
        }

        EditorUtility.CopySerialized(AnimatorStateEventBehaviourCopier.CopiedBehaviours, this);
    }

    [ContextMenu("Paste as New")]
    protected void PasteAsNew()
    {
        if(AnimatorStateEventBehaviourCopier.CopiedBehaviours == null)
        {
            return;
        }

        StateMachineBehaviourContext[] aContexts = AnimatorController.FindStateMachineBehaviourContext(this);
        if(aContexts == null)
        {
            return;
        }

        StateMachineBehaviour aCopiedTarget = null;
        AnimatorState state = aContexts[0].animatorObject as AnimatorState;
        if(state != null)
        {
            aCopiedTarget = state.AddStateMachineBehaviour(AnimatorStateEventBehaviourCopier.CopiedBehaviours.GetType());
        }
        else
        {
            AnimatorStateMachine aSubMachine = aContexts[0].animatorObject as AnimatorStateMachine;
            if(aSubMachine != null)
            {
                aCopiedTarget = aSubMachine.AddStateMachineBehaviour(AnimatorStateEventBehaviourCopier.CopiedBehaviours.GetType());
            }
        }

        if(aCopiedTarget != null)
        {
            EditorUtility.CopySerialized(AnimatorStateEventBehaviourCopier.CopiedBehaviours, aCopiedTarget);
        }
    }
#endif
#endregion
}