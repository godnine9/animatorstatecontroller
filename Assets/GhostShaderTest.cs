﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class GhostShaderTest : MonoBehaviour {

    public float Duration = 1;
    public float StartScale = 1;
    public float EndScale = 1;
    public Color32 StartColor = Color.white;
    public Color32 EndColor = Color.white;
    private SkinnedMeshRenderer[] Meshs;

    // Use this for initialization
    void Start () {
        Meshs = GetComponentsInChildren<SkinnedMeshRenderer>();
    }

    public void SpawnGhost()
    {
        GameObject Ghost = new GameObject();
        MeshFilter[] GhostMeshFilters = gameObject.GetComponentsInChildren<MeshFilter>();
        Ghost.name = "Ghost";
        for (int index = 0; index < Meshs.Length; index++)
        {
            GameObject aGhostMesh = new GameObject();
            MeshRenderer aMeshRenderer = aGhostMesh.AddComponent<MeshRenderer>();
            MeshFilter aMeshFilter = aGhostMesh.AddComponent<MeshFilter>();
            Material GhostMat = new Material(Shader.Find("Custom/GhostShader_v02"));
            aGhostMesh.transform.localScale = new Vector3(StartScale, StartScale, StartScale);
            GhostMat.SetColor("_Color", StartColor);
            GhostMat.SetFloat("_FresnelScale", 2.0f);
            GhostMat.SetFloat("_FresnelPower", 1.8f);
            GhostMat.SetFloat("_Opacity", 1.8f);
            aMeshRenderer.material = GhostMat;
            Mesh aMesh = new Mesh();
            Meshs[index].BakeMesh(aMesh);
            aMeshFilter.mesh = aMesh;
            aGhostMesh.transform.position = Meshs[index].transform.position;
            aGhostMesh.transform.rotation = Meshs[index].transform.rotation;
            aMeshRenderer.material.DOColor(EndColor, "_Color", Duration);
            aMeshRenderer.material.DOFloat(0.0f, "_Opacity", Duration);
            aGhostMesh.transform.DOScale(EndScale,Duration);
            aGhostMesh.transform.SetParent(Ghost.transform);
        }
        Destroy(Ghost, Duration);
    }
}
